#!/bin/bash
# Script to create channel block 0 and then create channel
cp $FABRIC_CFG_PATH/core.yaml /vars/core.yaml
cd /vars
export FABRIC_CFG_PATH=/vars
configtxgen -profile OrgChannel \
  -outputCreateChannelTx estatechannel.tx -channelID estatechannel

export CORE_PEER_TLS_ENABLED=true
export CORE_PEER_ID=cli
export CORE_PEER_ADDRESS=192.168.85.146:7004
export CORE_PEER_TLS_ROOTCERT_FILE=/vars/keyfiles/peerOrganizations/verificationAuthority.estate.com/peers/peer1.verificationAuthority.estate.com/tls/ca.crt
export CORE_PEER_LOCALMSPID=verificationAuthority-estate-com
export CORE_PEER_MSPCONFIGPATH=/vars/keyfiles/peerOrganizations/verificationAuthority.estate.com/users/Admin@verificationAuthority.estate.com/msp
export ORDERER_ADDRESS=192.168.85.146:7010
export ORDERER_TLS_CA=/vars/keyfiles/ordererOrganizations/estate.com/orderers/orderer2.estate.com/tls/ca.crt
peer channel create -c estatechannel -f estatechannel.tx -o $ORDERER_ADDRESS \
  --cafile $ORDERER_TLS_CA --tls
