#!/bin/bash
cd $FABRIC_CFG_PATH
# cryptogen generate --config crypto-config.yaml --output keyfiles
configtxgen -profile OrdererGenesis -outputBlock genesis.block -channelID systemchannel

configtxgen -printOrg buyer-estate-com > JoinRequest_buyer-estate-com.json
configtxgen -printOrg seller-estate-com > JoinRequest_seller-estate-com.json
configtxgen -printOrg verificationAuthority-estate-com > JoinRequest_verificationAuthority-estate-com.json
