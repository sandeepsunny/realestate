#!/bin/bash
# Script to join a peer to a channel
export CORE_PEER_TLS_ENABLED=true
export CORE_PEER_ID=cli
export CORE_PEER_ADDRESS=192.168.85.146:7005
export CORE_PEER_TLS_ROOTCERT_FILE=/vars/keyfiles/peerOrganizations/buyer.estate.com/peers/peer1.buyer.estate.com/tls/ca.crt
export CORE_PEER_LOCALMSPID=buyer-estate-com
export CORE_PEER_MSPCONFIGPATH=/vars/keyfiles/peerOrganizations/buyer.estate.com/users/Admin@buyer.estate.com/msp
export ORDERER_ADDRESS=192.168.85.146:7009
export ORDERER_TLS_CA=/vars/keyfiles/ordererOrganizations/estate.com/orderers/orderer1.estate.com/tls/ca.crt
if [ ! -f "estatechannel.genesis.block" ]; then
  peer channel fetch oldest -o $ORDERER_ADDRESS --cafile $ORDERER_TLS_CA \
  --tls -c estatechannel /vars/estatechannel.genesis.block
fi

peer channel join -b /vars/estatechannel.genesis.block \
  -o $ORDERER_ADDRESS --cafile $ORDERER_TLS_CA --tls
