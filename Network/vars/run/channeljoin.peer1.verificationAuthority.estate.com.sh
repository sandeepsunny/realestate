#!/bin/bash
# Script to join a peer to a channel
export CORE_PEER_TLS_ENABLED=true
export CORE_PEER_ID=cli
export CORE_PEER_ADDRESS=192.168.85.146:7004
export CORE_PEER_TLS_ROOTCERT_FILE=/vars/keyfiles/peerOrganizations/verificationAuthority.estate.com/peers/peer1.verificationAuthority.estate.com/tls/ca.crt
export CORE_PEER_LOCALMSPID=verificationAuthority-estate-com
export CORE_PEER_MSPCONFIGPATH=/vars/keyfiles/peerOrganizations/verificationAuthority.estate.com/users/Admin@verificationAuthority.estate.com/msp
export ORDERER_ADDRESS=192.168.85.146:7010
export ORDERER_TLS_CA=/vars/keyfiles/ordererOrganizations/estate.com/orderers/orderer2.estate.com/tls/ca.crt
if [ ! -f "estatechannel.genesis.block" ]; then
  peer channel fetch oldest -o $ORDERER_ADDRESS --cafile $ORDERER_TLS_CA \
  --tls -c estatechannel /vars/estatechannel.genesis.block
fi

peer channel join -b /vars/estatechannel.genesis.block \
  -o $ORDERER_ADDRESS --cafile $ORDERER_TLS_CA --tls
