
let profile = {

    seller: {
    
    "Wallet":"../Network/vars/profiles/vscode/wallets/seller.estate.com",
    
    "CP": "../Network/vars/profiles/estatechannel_connection_for_nodesdk.json"
    
    },
    
    verificationAuthority : {
    
    "Wallet":"../Network/vars/profiles/vscode/wallets/verificationAuthority.estate.com",
    
    "CP": "../Network/vars/profiles/estatechannel_connection_for_nodesdk.json"
    
    },
    
    buyer:{
    
    "Wallet":"../Network/vars/profiles/vscode/wallets/buyer.estate.com",
    
    "CP": "../Network/vars/profiles/estatechannel_connection_for_nodesdk.json"
    
    }
    
    }
    
    
    
    module.exports = {
    
    profile
    
    }