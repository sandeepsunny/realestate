var express = require('express');
var router = express.Router();
const { ClientApplication } = require('./client');



const { EventListener } = require('./events')

let BuyerEvent = new EventListener();
BuyerEvent.txnEventListener("buyer", "Admin", "estatechannel",
    "Chaincodelatest", "BuyContract", "buyRequest","20","748","ggef","6767");


// eventClient.contractEventListner("seller", "Admin", "estatechannel",
//     "Chaincodelatest", "PropertyContract", "addPropertyEvent");

// eventClient.blockEventListner("seller", "Admin", "estatechannel", "Chaincodelatest");

router.get("/", (req, res) => {
    res.send("hello");
});

router.get('/getAllSaleProperties', function (req, res) {
    let buyerClient = new ClientApplication();

    buyerClient.generateAndSubmitTxn(
        "buyer",
        "Admin",
        "estatechannel",
        "Chaincodelatest",
        "BuyContract",
        "queryTxn",
        "",
        "getAllSaleProperties"
    )
        .then(properties => {
            const dataBuffer = properties.toString();
            console.log("properties are ", properties.toString())
            const value = JSON.parse(dataBuffer)
            console.log("History DataBuffer is", value)
            res.send(value);
        }).catch(err => {
            res.send({ message: "Error while fetching properties", err });
        })
});



router.get('/readMyBuyRequest/:requestId', async function (req, res) {
    const requestId = req.params.requestId;

    let buyerClient = new ClientApplication();

    buyerClient.generateAndSubmitTxn(
        "buyer",
        "Admin",
        "estatechannel",
        "Chaincodelatest",
        "BuyContract",
        "queryTxn",
        "",
        "readMyBuyRequest",
        requestId
    ).then(message => {
        console.log("Message response", message)
        messgae = JSON.stringify(message);
        message = JSON.parse(message)
        res.json(message);
    }).catch((err) => {
        res.json({ message: `Error while fetching details of seller ${requestId}` });
    });

});
router.get('/buyRequestExists/:requestId', async function (req, res) {
  const requestId = req.params.requestId;

  let buyerClient = new ClientApplication();

  buyerClient.generateAndSubmitTxn(
      "buyer",
      "Admin",
      "estatechannel",
      "Chaincodelatest",
      "BuyContract",
      "queryTxn",
      "",
      "buyExists",
      requestId
  ).then(message => {
      console.log("Message response", message)
      messgae = JSON.stringify(message);
      message = JSON.parse(message)
      res.json(message);
  }).catch((err) => {
      res.json({ message: `Error while fetching details of seller ${requestId}` });
  });

});



router.post('/buyRequest', async function (req, res) {
  const propertyId = req.body.propertyId
  const ownerId=req.body.ownerId
  const ownerName=req.body.ownerName
  const amountSent=req.body.amountSent

   
    let buyerClient = new ClientApplication();

    buyerClient.generateAndSubmitTxn(
      "buyer",
      "Admin",
      "estatechannel",
      "Chaincodelatest",
      "BuyContract",
      "invokeTxn",
      "",
       "buyRequest",

        propertyId,ownerId,ownerName,amountSent)
        .then(message => {
            res.status(200).send("Request sent Successfully")
        }).catch(error => {
            res.status(500).send({ error: `Failed to add new product`, message: `${error}` })
        });
});

router.post('/createProperty', async function (req, res) {
  const propertyId = req.body.propertyId;
  const ownerId=req.body.ownerId;
  const ownerName=req.body.ownerName;
  const propertyType=req.body.propertyType;
  const price =req.body.price;
  const location=req.body.location;
  
  let sellerClient = new ClientApplication();

  const transientData = {
      ownerId: Buffer.from(ownerId),
      ownerName: Buffer.from(ownerName),
      propertyType: Buffer.from(propertyType),
      price:Buffer.from(price),
      location:Buffer.from(location)
    
  }

  sellerClient.generateAndSubmitTxn(
      "seller",
      "Admin",
      "estatechannel",
      "Chaincodelatest",
      "PropertyContract",
      "privateTxn",
      transientData,
      "createProperty",
      propertyId
  ).then(message => {
      res.status(200).send("property Registered successfully")
  }).catch(error => {
      res.status(500).send({ error: `Failed to register property`, message: `${error}` })
  });
});



router.get('/readMyProperty/:propertyId', async function (req, res) {

  const propertyId = req.params.propertyId;
  let sellerClient = new ClientApplication();

  sellerClient.generateAndSubmitTxn(
      "seller",
      "Admin",
      "estatechannel",
      "Chaincodelatest",
      "PropertyContract",
      "queryTxn",
      "",
      "readMyProperty",
      propertyId
  ).then(message => {
    console.log("Message response", message)
    messgae = JSON.stringify(message);
    message = JSON.parse(message)
    res.json(message);
}).catch((err) => {
    res.json({ message: `Error while fetching details of property ${propertyId}` });
});

});
router.get('/readMySellingProperty/:propertyId', async function (req, res) {

  const propertyId = req.params.propertyId;
  let sellerClient = new ClientApplication();

  sellerClient.generateAndSubmitTxn(
      "seller",
      "Admin",
      "estatechannel",
      "Chaincodelatest",
      "PropertyContract",
      "queryTxn",
      "",
      "readMySellingProperty",
      propertyId
  ).then(message => {
    console.log("Message response", message)
    messgae = JSON.stringify(message);
    message = JSON.parse(message)
    res.json(message);
}).catch((err) => {
    res.json({ message: `Property your looking is not present or it might be deleted with  propertyId : ${propertyId}` });
});

});



router.put('/updatePropertyPrice', async function (req, res) {
  const propertyId = req.body.propertyId
  const newPrice=req.body.newPrice

   
    let SellerClient = new ClientApplication();

    SellerClient.generateAndSubmitTxn(
      "seller",
      "Admin",
      "estatechannel",
      "Chaincodelatest",
      "PropertyContract",
      "invokeTxn",
      "",
       "updatePropertyPrice",

        propertyId,newPrice)
        .then(message => {
            res.status(200).send("Request sent Successfully")
        }).catch(error => {
            res.status(500).send({ error: `Failed to add new product`, message: `${error}` })
        });
});
router.delete('/removepropertyFromSale/:propertyId', async function (req, res) {
 
  const propertyId = req.params.propertyId;


   
    let SellerClient = new ClientApplication();

    SellerClient.generateAndSubmitTxn(
      "seller",
      "Admin",
      "estatechannel",
      "Chaincodelatest",
      "PropertyContract",
      "invokeTxn",
      "",
       "removepropertyFromSale",

        propertyId)
        .then(message => {
            res.status(200).send("Property removed Successfully from selling list")
        }).catch(error => {
            res.status(500).send({ error: `Failed to delete property`, message: `${error}` })
        });
});



router.get('/queryAllBuyRequests', function (req, res) {
  let verificationClient = new ClientApplication();

  verificationClient.generateAndSubmitTxn(
      "verificationAuthority", 
      "Admin",
      "estatechannel",
      "Chaincodelatest",
      "ListingContract",
      "queryTxn",
      "",
      "queryAllBuyRequests"
  )
      .then(properties => {
          const dataBuffer = properties.toString();
          console.log("properties are ", properties.toString())
          const value = JSON.parse(dataBuffer)
          console.log("History DataBuffer is", value)
          res.send(value);
      }).catch(err => {
          res.send({ message: "Error while fetching properties", err });
      })
});




router.post('/transferProperty', async function (req, res) {
  const propertyId = req.body.propertyId
  const newOwnerId=req.body.newOwnerId
  const newOwnerName=req.body.newOwnerName
  const amountSent=req.body.amountSent

   
    let verificationClient = new ClientApplication();

    verificationClient.generateAndSubmitTxn(
      "verificationAuthority",
      "Admin",
      "estatechannel",
      "Chaincodelatest",
      "ListingContract",
      "invokeTxn",
      "",
       "transferProperty",
        propertyId,newOwnerId,newOwnerName,amountSent)
        .then(message => {
            res.status(200).send(message.toString())
        }).catch(error => {
            res.status(500).send({ error: `Failed to transfer`, message: `${error}` })
        });
});



router.get('/readListing/:propertyId', function (req, res) {
    const propertyId = req.params.propertyId;
    let verificationClient = new ClientApplication();
  
    verificationClient.generateAndSubmitTxn(
        "verificationAuthority", 
        "Admin",
        "estatechannel",
        "Chaincodelatest",
        "ListingContract",
        "queryTxn",
        "",
        "readListing",
        propertyId
    )
        .then(properties => {
            const dataBuffer = properties.toString();
            console.log("properties are ", properties.toString())
            const value = JSON.parse(dataBuffer)
            console.log("properties", value)
            res.send(value);
        }).catch(err => {
            res.send({ message: "Error while fetching properties", err });
        })
  });
  router.get('/getAllSalePropertiesFromVefication', function (req, res) {
    let buyerClient = new ClientApplication();

    buyerClient.generateAndSubmitTxn(
        "verificationAuthority",
        "Admin",
        "estatechannel",
        "Chaincodelatest",
        "BuyContract",
        "queryTxn",
        "",
        "getAllSaleProperties"
    )
        .then(properties => {
            const dataBuffer = properties.toString();
            console.log("properties are ", properties.toString())
            const value = JSON.parse(dataBuffer)
            console.log("History DataBuffer is", value)
            res.send(value);
        }).catch(err => {
            res.send({ message: "Error while fetching properties", err });
        })
});
// getPropertyHistory
router.get('/getPropertyHistory/:propertyId', function (req, res) {
    const propertyId = req.params.propertyId;
    let verificationClient = new ClientApplication();

    verificationClient.generateAndSubmitTxn(
        "verificationAuthority",
        "Admin",
        "estatechannel",
        "Chaincodelatest",
        "ListingContract",
        "queryTxn",
        "",
        "getPropertyHistory",propertyId
    )
        .then(properties => {
            const dataBuffer = properties.toString();
            console.log("properties are ", properties.toString())
            const value = JSON.parse(dataBuffer)
            console.log("History DataBuffer is", value)
            res.send(value);
        }).catch(err => {
            res.send({ message: "Error while fetching properties", err });
        })
});


module.exports = router;
