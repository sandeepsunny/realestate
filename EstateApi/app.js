const express = require("express");
const app = express();
const cookieParser = require('cookie-parser');
const indexRouter = require('./routes/index');

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use("/", indexRouter);
// app.use(cookieParser());


app.listen("3000", () => {
    console.log(`Application running on port 3000`)
});
