/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const PropertyContract = require('./lib/property-contract');
const ListingContract=require('./lib/listing-contract')
const BuyContract=require('./lib/buy-contract')
module.exports.ListingContract=ListingContract;
module.exports.PropertyContract = PropertyContract;
module.exports.BuyContract=BuyContract
module.exports.contracts = [ PropertyContract ,ListingContract,BuyContract];