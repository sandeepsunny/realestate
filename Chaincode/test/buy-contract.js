/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const { ChaincodeStub, ClientIdentity } = require('fabric-shim');
const { BuyContract } = require('..');
const winston = require('winston');

const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');

chai.should();
chai.use(chaiAsPromised);
chai.use(sinonChai);

class TestContext {

    constructor() {
        this.stub = sinon.createStubInstance(ChaincodeStub);
        this.clientIdentity = sinon.createStubInstance(ClientIdentity);
        this.logger = {
            getLogger: sinon.stub().returns(sinon.createStubInstance(winston.createLogger().constructor)),
            setLevel: sinon.stub(),
        };
    }

}

describe('BuyContract', () => {

    let contract;
    let ctx;

    beforeEach(() => {
        contract = new BuyContract();
        ctx = new TestContext();
        ctx.stub.getState.withArgs('1001').resolves(Buffer.from('{"value":"buy 1001 value"}'));
        ctx.stub.getState.withArgs('1002').resolves(Buffer.from('{"value":"buy 1002 value"}'));
    });

    describe('#buyExists', () => {

        it('should return true for a buy', async () => {
            await contract.buyExists(ctx, '1001').should.eventually.be.true;
        });

        it('should return false for a buy that does not exist', async () => {
            await contract.buyExists(ctx, '1003').should.eventually.be.false;
        });

    });

    describe('#createBuy', () => {

        it('should create a buy', async () => {
            await contract.createBuy(ctx, '1003', 'buy 1003 value');
            ctx.stub.putState.should.have.been.calledOnceWithExactly('1003', Buffer.from('{"value":"buy 1003 value"}'));
        });

        it('should throw an error for a buy that already exists', async () => {
            await contract.createBuy(ctx, '1001', 'myvalue').should.be.rejectedWith(/The buy 1001 already exists/);
        });

    });

    describe('#readBuy', () => {

        it('should return a buy', async () => {
            await contract.readBuy(ctx, '1001').should.eventually.deep.equal({ value: 'buy 1001 value' });
        });

        it('should throw an error for a buy that does not exist', async () => {
            await contract.readBuy(ctx, '1003').should.be.rejectedWith(/The buy 1003 does not exist/);
        });

    });

    describe('#updateBuy', () => {

        it('should update a buy', async () => {
            await contract.updateBuy(ctx, '1001', 'buy 1001 new value');
            ctx.stub.putState.should.have.been.calledOnceWithExactly('1001', Buffer.from('{"value":"buy 1001 new value"}'));
        });

        it('should throw an error for a buy that does not exist', async () => {
            await contract.updateBuy(ctx, '1003', 'buy 1003 new value').should.be.rejectedWith(/The buy 1003 does not exist/);
        });

    });

    describe('#deleteBuy', () => {

        it('should delete a buy', async () => {
            await contract.deleteBuy(ctx, '1001');
            ctx.stub.deleteState.should.have.been.calledOnceWithExactly('1001');
        });

        it('should throw an error for a buy that does not exist', async () => {
            await contract.deleteBuy(ctx, '1003').should.be.rejectedWith(/The buy 1003 does not exist/);
        });

    });

});
