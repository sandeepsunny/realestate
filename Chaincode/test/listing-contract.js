/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const { ChaincodeStub, ClientIdentity } = require('fabric-shim');
const { ListingContract } = require('..');
const winston = require('winston');

const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');

chai.should();
chai.use(chaiAsPromised);
chai.use(sinonChai);

class TestContext {

    constructor() {
        this.stub = sinon.createStubInstance(ChaincodeStub);
        this.clientIdentity = sinon.createStubInstance(ClientIdentity);
        this.logger = {
            getLogger: sinon.stub().returns(sinon.createStubInstance(winston.createLogger().constructor)),
            setLevel: sinon.stub(),
        };
    }

}

describe('ListingContract', () => {

    let contract;
    let ctx;

    beforeEach(() => {
        contract = new ListingContract();
        ctx = new TestContext();
        ctx.stub.getState.withArgs('1001').resolves(Buffer.from('{"value":"listing 1001 value"}'));
        ctx.stub.getState.withArgs('1002').resolves(Buffer.from('{"value":"listing 1002 value"}'));
    });

    describe('#listingExists', () => {

        it('should return true for a listing', async () => {
            await contract.listingExists(ctx, '1001').should.eventually.be.true;
        });

        it('should return false for a listing that does not exist', async () => {
            await contract.listingExists(ctx, '1003').should.eventually.be.false;
        });

    });

    describe('#createListing', () => {

        it('should create a listing', async () => {
            await contract.createListing(ctx, '1003', 'listing 1003 value');
            ctx.stub.putState.should.have.been.calledOnceWithExactly('1003', Buffer.from('{"value":"listing 1003 value"}'));
        });

        it('should throw an error for a listing that already exists', async () => {
            await contract.createListing(ctx, '1001', 'myvalue').should.be.rejectedWith(/The listing 1001 already exists/);
        });

    });

    describe('#readListing', () => {

        it('should return a listing', async () => {
            await contract.readListing(ctx, '1001').should.eventually.deep.equal({ value: 'listing 1001 value' });
        });

        it('should throw an error for a listing that does not exist', async () => {
            await contract.readListing(ctx, '1003').should.be.rejectedWith(/The listing 1003 does not exist/);
        });

    });

    describe('#updateListing', () => {

        it('should update a listing', async () => {
            await contract.updateListing(ctx, '1001', 'listing 1001 new value');
            ctx.stub.putState.should.have.been.calledOnceWithExactly('1001', Buffer.from('{"value":"listing 1001 new value"}'));
        });

        it('should throw an error for a listing that does not exist', async () => {
            await contract.updateListing(ctx, '1003', 'listing 1003 new value').should.be.rejectedWith(/The listing 1003 does not exist/);
        });

    });

    describe('#deleteListing', () => {

        it('should delete a listing', async () => {
            await contract.deleteListing(ctx, '1001');
            ctx.stub.deleteState.should.have.been.calledOnceWithExactly('1001');
        });

        it('should throw an error for a listing that does not exist', async () => {
            await contract.deleteListing(ctx, '1003').should.be.rejectedWith(/The listing 1003 does not exist/);
        });

    });

});
