/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const { Contract } = require('fabric-contract-api');

const PropertyContract = require('./property-contract');
async function getCollectionName(ctx) {
    const collectionName = 'CollectionProperty';
    return collectionName;
}



class ListingContract extends Contract {

    async listingExists(ctx, listingId) {
        const buffer = await ctx.stub.getState(listingId);
        return (!!buffer && buffer.length > 0);
    }

   


    async readListing(ctx, listingId) {
        const exists = await this.listingExists(ctx, listingId);
        if (!exists) {
            throw new Error(`The listing ${listingId} does not exist`);
        }
        const buffer = await ctx.stub.getState(listingId);
        const asset = JSON.parse(buffer.toString());
        return asset;
    }

  



    
    async queryAllProperties(ctx){
        const queryString={
            selector:{
                assetType:'forSale'
            },
           // sort:[{color:'asc'}]
        }
        let resultIterator= await ctx.stub.getQueryResult(JSON.stringify(queryString))
        let result=await this.getAllResults(resultIterator,false)
        return JSON.stringify(result)
    }
    async getAllResults(iterator,isHistory){
        let allResult=[]
        for(let res=await iterator.next();!res.done;res=await iterator.next()){
            if(res.value&&res.value.value.toString()){
                let jsonRes={}
                if(isHistory&&isHistory==true){
                jsonRes.TxId=res.value.txId 
                jsonRes.TimeStamp=res.value.timestamp
                jsonRes.Record=JSON.parse(res.value.value.toString())
                }
                else{
                jsonRes.Key=res.value.key 
                jsonRes.Record=JSON.parse(res.value.value.toString())
                }
                allResult.push(jsonRes)
            }
        }
        await iterator.close()
        return allResult
    }

   

      
     async transferProperty(ctx, propertyId, newOwnerId,newOwnerName,amountSent) {
        const collectionName = await getCollectionName(ctx);
        // Verify that the transaction is invoked by the verification authority
        if (ctx.clientIdentity.getMSPID() !== 'verificationAuthority-estate-com') {
          throw new Error('Access denied. Only the Verification Authority can transfer properties.');
        }


    
        // Retrieve the property from the private data collection of SellerBuyerContract
        let propertyBytes = await ctx.stub.getPrivateData(collectionName, propertyId);
        if (!propertyBytes || propertyBytes.length === 0) {
          throw new Error(`Property ${propertyId} does not exist`);
        }
      
    
        // Update the owner ID of the property
        let property = JSON.parse(propertyBytes.toString());
        if(amountSent==property.price){
        property.ownerId = newOwnerId;
        property.ownerName=newOwnerName;
    
        // Save the updated property in the private data collection
        await ctx.stub.putPrivateData(collectionName, propertyId, Buffer.from(JSON.stringify(property)));

        //update it in public ledger
        let propertyBytes1 = await ctx.stub.getState( propertyId);
        let property1 = JSON.parse(propertyBytes1.toString());
        property1.ownerName=newOwnerName;
        await ctx.stub.putState(propertyId, Buffer.from(JSON.stringify(property1)));
        let addPropertyEventData = { Type: 'property transfered', PropertyType: property1.propertyType};
        await ctx.stub.setEvent('addPropertyEvent', Buffer.from(JSON.stringify(addPropertyEventData)));
    
        return "Transfer successfull"
        }else{
            return "sorry you have not paid enough amount your amount will be refunded"
        }
    }

    async queryAllBuyRequests(ctx){
        const queryString={
            selector:{
                assetType:'request'
            },
           
        }
        let resultIterator= await ctx.stub.getQueryResult(JSON.stringify(queryString))
        let result=await this.getAllResults(resultIterator,false)
        return JSON.stringify(result)
    }

    async getPropertyHistory(ctx,propertyId){
        if (ctx.clientIdentity.getMSPID() !== 'verificationAuthority-estate-com') {
            throw new Error('Access denied. Only the Verification Authority can transfer properties.');
        }
        let resultIterator= await ctx.stub.getHistoryForKey(propertyId)
        let result=await this.getAllResults(resultIterator,true)
        return JSON.stringify(result)
        
    }



        




      



    

}

module.exports = ListingContract;
