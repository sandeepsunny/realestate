/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const { Contract } = require('fabric-contract-api');
const crypto = require('crypto');
//const ListingContract=require('./listing-contract')




async function getCollectionName(ctx) {
    const collectionName = 'CollectionProperty';
    return collectionName;
}

class PropertyContract extends Contract {

    async propertyExists(ctx, propertyId) {
        const collectionName = await getCollectionName(ctx);
        const data = await ctx.stub.getPrivateDataHash(collectionName, propertyId);
        return (!!data && data.length > 0);
    }
    async propertyExistsInSaleList(ctx, propertyId) {
        const buffer = await ctx.stub.getState(propertyId);
        return (!!buffer && buffer.length > 0);
    }


    async createProperty(ctx, propertyId) {
        const mspID = ctx.clientIdentity.getMSPID();
        if (mspID === 'seller-estate-com') {

            const exists = await this. propertyExists(ctx, propertyId);
            if (exists) {
                throw new Error(`The asset  property ${propertyId} already exists`);
            }

     
        const  propertyAsset = {};

        const transientData = ctx.stub.getTransient();
        if (transientData.size === 0 || !transientData.has('ownerId') ||
            !transientData.has('ownerName') ||
            !transientData.has('propertyType') ||
            !transientData.has('price') ||
            !transientData.has('location')
        ) {
            throw new Error('The privateValue key was not specified in transient data. Please try again.');
        }
         propertyAsset.ownerId = transientData.get('ownerId').toString();
         propertyAsset.ownerName = transientData.get('ownerName').toString();
         propertyAsset.propertyType = transientData.get('propertyType').toString();
         propertyAsset.price = transientData.get('price').toString();
         propertyAsset.location = transientData.get('location').toString();
        // propertyAsset.ownerName = transientData.get('ownerName').toString();
         propertyAsset.assetType = ' property'

        const collectionName = await getCollectionName(ctx);

        await ctx.stub.putPrivateData(collectionName, propertyId, Buffer.from(JSON.stringify( propertyAsset)));


        //adding in public ledger
    const asset = { ownerName:  propertyAsset.ownerName,propertyType: propertyAsset.propertyType,price:propertyAsset.price,location:propertyAsset.location,assetType:"forSale"};
    const buffer = Buffer.from(JSON.stringify(asset));
    await ctx.stub.putState(propertyId, buffer);
    let addPropertyEventData = { Type: 'property created', PropertyType: propertyAsset.propertyType};
    await ctx.stub.setEvent('addPropertyEvent', Buffer.from(JSON.stringify(addPropertyEventData)));
    return "Hurray !! New Property Registered Successfully"

        }

        else {
            return `User under the following MSP: ${mspID} cannot perform this action`
        }

    }

    async readMyProperty(ctx, propertyId) {
        const exists = await this.propertyExists(ctx, propertyId);
        if (!exists) {
            throw new Error(`The asset property ${propertyId} does not exist`);
        }
        let privateDataString;
        const collectionName = await getCollectionName(ctx);
        const privateData = await ctx.stub.getPrivateData(collectionName, propertyId);
        privateDataString = JSON.parse(privateData.toString());
       return privateDataString;

    }

    async updatePropertyPrice(ctx, propertyId,newPrice) {

        const mspID = ctx.clientIdentity.getMSPID();
        if (mspID === 'seller-estate-com') {
        const exists = await this.propertyExists(ctx, propertyId);
        if (!exists) {
            throw new Error(`The asset property ${propertyId} does not exist`);
        }

        const collectionName = await getCollectionName(ctx);

        let propertyBytes = await ctx.stub.getPrivateData(collectionName, propertyId);
     
        let property = JSON.parse(propertyBytes.toString());
        property.price=newPrice
        await ctx.stub.putPrivateData(collectionName, propertyId, Buffer.from(JSON.stringify(property)));
        //updating in sale products list as well

        let propertyBytes1 = await ctx.stub.getState( propertyId);
        let property1 = JSON.parse(propertyBytes1.toString());
        property1.price=newPrice;
        await ctx.stub.putState(propertyId, Buffer.from(JSON.stringify(property1)));
        return "Price has been updated !!!!"
     }else {
        return `User under the following MSP: ${mspID} cannot perform this action`
    }
 

    }

    async removepropertyFromSale(ctx, propertyId) {

        const mspID = ctx.clientIdentity.getMSPID();
        if (mspID === 'seller-estate-com') {

        const exists = await this.propertyExistsInSaleList(ctx, propertyId);
        if (!exists) {
            throw new Error(`The asset property ${propertyId} does not exist`);
        }
        const collectionName = await getCollectionName(ctx);
        await ctx.stub.deletePrivateData(collectionName, propertyId);
        await ctx.stub.deleteState(propertyId);
        return "Your property has been removed from Sale's list"
       }
        else {
            return `User under the following MSP: ${mspID} cannot perform this action`
        }
     

    }

    async readMySellingProperty(ctx, buyId) {
        const exists = await this.propertyExistsInSaleList(ctx, buyId);
        if (!exists) {
            throw new Error(`The buy ${buyId} does not exist`);
        }
        const buffer = await ctx.stub.getState(buyId);
        const asset = JSON.parse(buffer.toString());
        return asset;
    }





}

module.exports = PropertyContract;

/*

{
    "ownerId" : "1234",
     "ownerName" : "sandy",
     "propertyType" : "land",
     "price" : "23323",
     "location" : "sangareddy"
    
}
*/
// async updateOwner(ctx, propertyId, newOwnerId,newOwnerName) {
    //     const collectionName = await getCollectionName(ctx);
    //     // Update the owner ID of the property in the private data collection
    //     let propertyBytes = await ctx.stub.getPrivateData(collectionName, propertyId);
    //     if (!propertyBytes || propertyBytes.length === 0) {
    //       throw new Error(`Private property ${propertyId} does not exist`);
    //     }
    
    //     let property = JSON.parse(propertyBytes.toString());
    //     property.ownerId = newOwnerId;
    //     property.ownerName=newOwnerName;
    
    //     await ctx.stub.putPrivateData(collectionName, propertyId, Buffer.from(JSON.stringify(property)));
    //   }

