/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const { Contract } = require('fabric-contract-api');
const ListingContract=require('./listing-contract');
//..
const PropertyContract=require('./property-contract')

class BuyContract extends Contract {

    async buyExists(ctx, buyId) {
        const buffer = await ctx.stub.getState(buyId);
        return (!!buffer && buffer.length > 0);
    }


    async deleteBuyRequest(ctx, buyId) {
          

        const mspID = ctx.clientIdentity.getMSPID();
        if (mspID === 'buyer-estate-com') {

        const exists = await this.buyExists(ctx, buyId);
        if (!exists) {
            throw new Error(`The buy ${buyId} does not exist`);
        }
        await ctx.stub.deleteState(buyId);
        return "buy request has been withdrawn"
      }else{
        return `User under the following MSP: ${mspID} cannot perform this action`

      }
    }
   
    
    async buyRequest(ctx,propertyId,ownerId,ownerName,amountSent){
        const mspID = ctx.clientIdentity.getMSPID();
        if (mspID === 'buyer-estate-com') {
        const asset = { propertyId,ownerId,ownerName,amountSent,assetType:'request' };
        const buffer = Buffer.from(JSON.stringify(asset));
        await ctx.stub.putState(ownerId, buffer);
        let addBuyEventData = { Type: 'Buy request raised',ownerName:ownerName};
        await ctx.stub.setEvent('addBuyEvent', Buffer.from(JSON.stringify(addBuyEventData)));
        }else{
            return `User under the following MSP: ${mspID} cannot perform this action`

        }
    

    }
    async readMyBuyRequest(ctx, buyId) {
        const mspID = ctx.clientIdentity.getMSPID();
        if (mspID === 'buyer-estate-com') {
        const exists = await this.buyExists(ctx, buyId);
        if (!exists) {
            throw new Error(`The buy ${buyId} does not exist`);
        }
        const buffer = await ctx.stub.getState(buyId);
        const asset = JSON.parse(buffer.toString());
        return asset;
    }else{
        return `User under the following MSP: ${mspID} cannot perform this action`
    }
    }
    async getAllSaleProperties(ctx){
        const queryString={
            selector:{
                assetType:'forSale'
            },
           
        }
        let resultIterator= await ctx.stub.getQueryResult(JSON.stringify(queryString))
        let result=await this.getAllResults(resultIterator,false)
        return JSON.stringify(result)
    }
    async getAllResults(iterator,isHistory){
        let allResult=[]
        for(let res=await iterator.next();!res.done;res=await iterator.next()){
            if(res.value&&res.value.value.toString()){
                let jsonRes={}
                if(isHistory&&isHistory==true){
                jsonRes.TxId=res.value.txId 
                jsonRes.TimeStamp=res.value.timestamp
                jsonRes.Record=JSON.parse(res.value.value.toString())
                }
                else{
                jsonRes.Key=res.value.key 
                jsonRes.Record=JSON.parse(res.value.value.toString())
                }
                allResult.push(jsonRes)
            }
        }
        await iterator.close()
        return allResult
    }


    


}

module.exports = BuyContract;


