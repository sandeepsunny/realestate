const { EventListener } = require('./events')

let SellerEvent = new EventListener();
SellerEvent.contractEventListener("seller", "Admin", "estatechannel",
    "Chaincodelatest", "PropertyContract", "addPropertyEvent");

let VerificationEvent=new EventListener()
VerificationEvent.contractEventListener("verificationAuthority","Admin","estatechannel","Chaincodelatest","ListingContract","addPropertyEvent")



let BuyEvent=new EventListener()
BuyEvent.contractEventListener("buyer","Admin","estatechannel","Chaincodelatest","BuyContract","addRequestEvent")